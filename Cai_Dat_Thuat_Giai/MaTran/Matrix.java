/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrix;

import java.util.Scanner;

/**
 *
 * @author thien
 */
public class Matrix {

    int i=0; int j=0;
    //n dong - m cot
    private int n, m;
    private double M[][];   
    
    public Matrix() {
    }
    
    public Matrix(int dong, int cot) {
        n = dong;
        m = cot;
        M = new double[n][m];

    }
    
    public Matrix(double M1[][]) {
        M = M1;
        n = M.length;
        m = M[0].length;
    }
    
    // copy constructor
    private Matrix(Matrix A) { this(A.M); }
    
    //doi cho 2 dong
    public void swap(int row1, int row2){
        double[] temp = M[row1];
        M[row1] = M[row2];
        M[row2] = temp;
    }
    
    //tao ma tran don vi NxN
    public Matrix matranDonVi(int n) {
        Matrix I = new Matrix(n, n);
        for (int i = 0; i < n; i++)
            I.M[i][i] = 1;
        return I;
    }
    
    //tao ma tran chuyen vi
    public Matrix matranChuyenVi() {
        Matrix A = new Matrix(n, m);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                A.M[j][i] = this.M[i][j];
        return A;
    }
    
    //cong hai ma tran C = A + B
    public Matrix congMaTran(Matrix B) {
        Matrix A = this;
        if (B.n != A.n || B.m != A.m) 
            throw new RuntimeException("2 ma tran khac kich thuoc.");
        Matrix C = new Matrix(n, m);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                C.M[i][j] = A.M[i][j] + B.M[i][j];
        return C;
    }


    //tru hai ma tran C = A - B
    public Matrix truMaTran(Matrix B) {
        Matrix A = this;
        if (B.n != A.n || B.m != A.m) 
            throw new RuntimeException("2 ma tran khac kich thuoc.");
        Matrix C = new Matrix(n, m);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                C.M[i][j] = A.M[i][j] - B.M[i][j];
        return C;
    }

    // kiem tra 2 ma tran bang nhau A=B?
    public boolean isEQ(Matrix B) {
        Matrix A = this;
        if (B.n != A.n || B.m != A.m) 
            throw new RuntimeException("2 ma tran khac kich thuoc.");
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                if (A.M[i][j] != B.M[i][j]) 
                    return false;
        return true;
    }

    // tim tich 2 ma tran C = A * B
    public Matrix times(Matrix B) {
        Matrix A = this;
        if (A.m != B.n) 
            throw new RuntimeException("2 ma tran kich thuoc khong hop le.");
        Matrix C = new Matrix(A.n, B.m);
        for (int i = 0; i < C.n; i++)
            for (int j = 0; j < C.m; j++)
                for (int k = 0; k < A.m; k++)
                    C.M[i][j] += (A.M[i][k] * B.M[k][j]);
        return C;
    }
    
    
    public void nhapMaTran() {
        Scanner input = new Scanner(System.in);
        System.out.print("Nhap vao so dong: ");
        n = input.nextInt();
        System.out.print("Nhap vao so cot: ");
        m = input.nextInt();
        M = new double[n][m]; 
        System.out.println("Nhap vao ma tran: ");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print("Nhap: [" + i + "] " + "[" + j + "] = ");
                M[i][j] = input.nextDouble();
            }
        }
    }
    
    public void inMaTran() {
        System.out.println("Ma tran: ");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print("  " + M[i][j]);
            }
            System.out.println("");
        }
    }
    
    public boolean ktraDoixung() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (M[i][j] != M[j][i]) {
                    return false;
                }
            }
        }
        return true;
    }
    
//    public Matrix congMatran(Matrix M1) {
//        Matrix C = new Matrix(n, m);
//        C.M= M1.M;
//
//        for (int i = 0; i < n; i++) {
//            for (int j = 0; j < m; j++) {
//                C.M[i][j] = M[i][j] + M1.M[i][j];
//                //System.out.println(" " + C[i][j]);
//            }
//            //System.out.println("");
//        }
//        //Matrix M2 = new Matrix(n, m);
//        return C;
//    }
    
    //tim ma tran x = A^-1 * b
//    public Matrix solve(Matrix rhs) {
//        if (m != n || rhs.n != m || rhs.m != 1)
//            throw new RuntimeException("2 ma tran kich thuoc khong hop le");
//
//        //A = THIS, b = rhs
//        Matrix A = new Matrix(this);
//        Matrix b = new Matrix(rhs);
//
//        // Gaussian elimination with partial pivoting
//        for (int i = 0; i < m; i++) {
//
//            //Tim phan tu lam chot va doi cho
//            int max = i;
//            for (int j = i + 1; j < m; j++)
//                if (Math.abs(A.M[j][i]) > Math.abs(A.M[max][i]))
//                    max = j;
//            A.swap(i, max);
//            b.swap(i, max);
//
//            // singular
//            if (A.M[i][i] == 0.0) 
//                throw new RuntimeException("Ma tran rong.");
//
//            // pivot within b
//            for (int j = i + 1; j < N; j++)
//                b.data[j][0] -= b.data[i][0] * A.data[j][i] / A.data[i][i];
//
//            // pivot within A
//            for (int j = i + 1; j < N; j++) {
//                double m = A.data[j][i] / A.data[i][i];
//                for (int k = i+1; k < N; k++) {
//                    A.data[j][k] -= A.data[i][k] * m;
//                }
//                A.data[j][i] = 0.0;
//            }
//        }
//
//        // back substitution
//        Matrix x = new Matrix(N, 1);
//        for (int j = N - 1; j >= 0; j--) {
//            double t = 0.0;
//            for (int k = j + 1; k < N; k++)
//                t += A.data[j][k] * x.data[k][0];
//            x.data[j][0] = (b.data[j][0] - t) / A.data[j][j];
//        }
//        return x;
//   
//    }
    
    //tim hang ma tran
    public int timHangMaTran(){
        //gan rank = so cot cua matrix
        int rank = m;
        
        for (int row = 0; row < rank; row++){
            
            if (M[row][row] != 0){
                for (int col = 0; col < n; col++)
                {
                    if (col != row){
                        double mult = (double)M[col][row] / M[row][row];
                        for (int i = 0; i < rank; i++)
                            M[col][i] -= mult * M[row][i];
                    }
                }
            }

            // Nhung phan tu tren duong cheo da bang 0 
            // 2 Truong hop:
            // 1) Neu dong duoi dong dang xet co phan tu khac 0 doi cho 2 dong do va xu ly dong dc doi cho
            // 2) Neu tat ca phan tu o cot dang xet ma duoi dong M[r][row] la 0, thi doi cho cot dang xet voi cot cuoi cung 
            //  va giam hang di 1
            else{
                boolean reduce = true;

                // Tim phan tu khac 0 cua cot hien tai
                for (int i = row + 1; i < n; i++)
                {
                    // Doi cho dong co phan tu khac 0 voi dong hien tai
                    if (M[i][row] != 0){
                        swap(row, i);
                        reduce = false;
                        break ;
                    }
                }

                // Neu khong tim thay dong nao co phan tu khac 0 cua cot hien tai nghia la tat ca phan tu cua cot nay la 0
                if (reduce){
                    //giam rank
                    rank--;

                    // Copy cot cuoi cung
                    for (int i = 0; i < n; i ++)
                            M[i][row] = M[i][rank];
                }

                row--;
            }
        }
        return rank;
}
    
    public Matrix timMaTranNghichDao(){
        double a[][] = new double [2*n][2*n];
        int k = 0, dau = 1;
        double x;
        
        for(int i = 0; i < n; i++)
            for(int j = 0; j < n; j++){
                a[i][j] = this.M[i][j];
            }
        
        for(int i = 0; i < n; i++)
            for(int j = n; j < 2*n; j++){
                if(i == (j-n)) 
                    a[i][j] = 1;
                else 
                    a[i][j] = 0;
            }
 
//        //Ham giai phuong trinh.--------------------
//        //Dung phep BDSC dua Aij ve ma tran cheo.
//        for(int j = 0; j < n; j++){
//            for(int i = n-1; i >= j; i--){
//                if(a[i][j] == 0) 
//                    continue;//Neu phan tu a bang 0 thi tang i.
//                if((i > j)&&(a[i][j] != 0)){
//                    k = i-1;
//                while((k >= j)&&(a[k][j] == 0)) 
//                    k--;//Xet cac phan tu a khac 0 tren cung cot j
//                if(k < j){
//                    //Doi cho dong thu i va j cho ca ma tran mo rong.
//                    for(int t = 0; t < 2*n; t++){
//                        double tam = a[i][t];
//                        a[i][t] = a[j][t];
//                        a[j][t]=tam;
//                    }
//                    dau=-dau;
//                }
//                if((k >= j)&&(k >= 0)){
//                    x = (double)-a[i][j] / a[k][j];
//                    //Nhan dong thu k cho x roi cong vao dong thu i.
//                    for(int t = 0; t < 2*n; t++)
//                        a[i][t] += a[k][t]*x;
//                    }
//                }
//            }
//        }
//        for(j = n-1; j >= 0; j--){
//            for(i = 0; i <= j; i++){
//                if(a[i][j] == 0) 
//                    continue;
//                if((i < j)&&(a[i][j] != 0)){
//                    k = i+1;
//                    while((k <= j)&&(a[k][j] == 0)) 
//                        k++;
//                    if((k <= j)&&(k < 2*n)){
//                        x = (double)-a[i][j] / a[k][j];
//                        //Nhan dong thu k voi x roi cong vao dong thu i.
//                        for(int t = 0; t < 2*n; t++)
//                            a[i][t] += a[k][t]*x;
//                    }
//                }
//            }
//        }
//        int det=1;
//        for(int i = 0; i < n; i++)
//            for(int j = 0; j < n; j++)
//                if(i == j) 
//                    det*=a[i][j];
//        if(det*dau != 0){
//            for(int i = 0; i < n; i++)
//                for(int j = n; j < 2*n; j++)
//                    a[i][j] = (double)a[i][j] / a[i][i]; 
//            Matrix b = this;
//            for(int i = 0; i < n; i++)
//                for(int j = n; j < 2*n; j++)
//                    b.M[i][j - n] = a[i][j];
//
//            return b;
//        }
//        return null;
        double t;
        for(int i = 0; i < n; i++){
            t=a[i][i];
            for(int j = i; j < 2*n; j++)
                a[i][j] = (double)a[i][j] / t;
            for(int j = 0; j < n; j++){
                if(i != j){
                   t = a[j][i];
                   for(k = 0; k < 2*n; k++)
                       a[j][k] = a[j][k] - t*a[i][k];
                }
            }
        }
        Matrix b = this;
        for(int i = 0; i < n; i++)
            for(int j = n; j < 2*n; j++)
                b.M[i][j - n] = a[i][j];

        return b;
    }
    
    public static void main(String[] args) {
        //ko kha nghich
//        double mat[][] = {{1, 7, 1, 3, 0},
//                       {1, 7, -1, -2, -2},
//                       {2, 14, 2, 7, 0},
//                       {6, 42, 3, 13, -3}};
        //ko kha nghich
//        double mat[][] = {{10, 20, 10},
//                       {-20, -30, 10},
//                       {30, 50, 0}};
        //ok
//        double mat[][] = {{1, 1, 2},
//                       {2, 3, 2},
//                       {1, 3, -1}};
        //ok
//        double mat[][] = {{1, 1, 2},
//                       {1, 2, 2},
//                       {1, 2, 3}};
        //ko kha nghich
//        double mat[][] = {{1, 2, 3},
//                       {-2, -5, 3},
//                       {2, 3, 15}};
        //ok
        double mat[][] = {{1, 2, 3, 4},
                       {2, 5, 4, 7},
                       {3, 7, 8, 12},
                       {4, 8, 14, 19}};

        Matrix a=new Matrix(mat);
        //a.nhapMaTran();
        a.inMaTran();
        
        
        Matrix b = new Matrix();
        //b = a.congMaTran(a);
        System.out.println("Ma tran nghich dao la: ");
        b = a.timMaTranNghichDao();
        b.inMaTran();
        
        
        System.out.println(a.timHangMaTran());
    }
}
    

