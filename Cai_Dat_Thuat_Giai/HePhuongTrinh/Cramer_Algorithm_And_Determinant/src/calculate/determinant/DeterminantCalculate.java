package calculate.determinant;

public class DeterminantCalculate {
	
	public static int execute(int[][] matrix) {	
		int n = matrix.length - 1;
	    if (n < 0)
	        return 0;
	    int M[][][] = new int[n + 1][][];
	    M[n] = matrix; // init first, largest, M to a
	    // create working arrays
	    for (int i = 0; i < n; i++)
	        M[i] = new int[i + 1][i + 1];
	    return execute(M, n);
	} // end method execute double [][] parameter
	
	public static int execute(int[][][] M, int m) {
	    if (m == 0)
	        return M[0][0][0];
	    int e = 1;
	    // init subarray to upper left mxm submatrix
	    for (int i = 0; i < m; i++)
	        for (int j = 0; j < m; j++)
	            M[m - 1][i][j] = M[m][i][j];
	    int sum = M[m][m][m] * execute(M, m - 1);
	    // walk through rest of rows of M
	    for (int i = m - 1; i >= 0; i--) {
	        for (int j = 0; j < m; j++)
	            M[m - 1][i][j] = M[m][i + 1][j];
	        e = -e;
	        sum += e * M[m][i][m] * execute(M, m - 1);
	    } // end for each row of matrix
	    return sum;
	} // end execute double [][][], int
	
}
