package GaussEliminationAlgorithm;

public class GaussAlgorithm {
	
	public static void main(String args[]) {
		
//		int n = 3;
//		
//		double matrix[][] = {
//				{1, 2, -1, 3},
//				{2, 5, -3, 6},
//				{-1, -1, 4, 1}
//			};
		
		int n = 4;
		double[][] matrix = new double[n][n];
        matrix[0] = new double[] { 2, -3, 5, -1, 2 };
        matrix[1] = new double[] { -1, -2, 3, 4, 0 };
        matrix[2] = new double[] { 3, 8, -5, 3, -2 };
        matrix[3] = new double[] { 0, -4, 2, -7, 9 };
		
		gauss(n, matrix);
	}
	
	public static void gauss(int n, double[][] detA) {
		
		double x[] = new double[n];
				
				//Pivotisation
				for(int i = 0; i < n; i++) {
					for(int k = i+1; k < n; k++) {
						if(Math.abs(detA[i][i]) < Math.abs(detA[k][i])) {
							for(int j = 0; j <= n; j++) {
								double temp = detA[i][j];
								detA[i][j] = detA[k][j];
								detA[k][j] = temp;
							}
						}
					}
				}
				
				System.out.println("\nThe matrix after Pivotisation is:\n");
		
				//print the new matrix
				for(int i = 0; i < n; i++) {
					for(int j = 0; j <= n; j++) {
						System.out.print(detA[i][j] + "\t");
					}
					System.out.println();
				}
				
				//loop to perform the gauss elimination
				for(int i = 0; i < n-1; i++) { 
					for(int k = i+1; k < n; k++) {
						double t = detA[k][i] / detA[i][i];
						for(int j = 0; j <= n; j++) {
							detA[k][j] = detA[k][j] - t*detA[i][j]; //make the elements below the pivot elements equal to zero or elimnate the variables
						}
					}
				}
				
				System.out.print("\n\nThe matrix after gauss-elimination is as follows:\n\n");
		
				//print the matrix
				for(int i = 0; i < n; i++) {
					for(int j = 0; j <= n; j++) {
						System.out.print(detA[i][j]+"\t");
					}
					System.out.println();
				}
				
				//back-substitution
				for(int i = n - 1; i >= 0; i--) {
					x[i] = detA[i][n]; //x is an array whose values correspond to the values of x,y,z..
					for(int j = i+1; j < n; j++) { //make the variable to be calculated equal to the rhs of the last equation
						if(j != i) { //then subtract all the lhs values except the coefficient of the variable whose value
							x[i] = x[i] - detA[i][j]*x[j];
						}
					}
					x[i] = x[i] / detA[i][i]; //now finally divide the rhs by the coefficient of the variable to be calculated
				}
				
				System.out.print("\nThe values of the variables are as follows:\n\n");
				for(int i = 0; i < n; i++) {
					System.out.print(x[i]+"\n");
				}
		
	}
	
}
